<?php

/**
 * @file
 * Checks file existence as a Rules condition
 */

/**
 * Implements hook_rules_condition_info().
 *
 * Checks for the existence of a file. Returns TRUE if the file exists.
 */
function file_exists_condition_rules_condition_info() {
  $items = array();
  $items['file_exists_condition_file_accessible'] = array(
    'label' => t('File is accessible'),
    'group' => t('System'),
    'parameter' => array(
      'file field' => array(
        'type' => 'file',
        'label' => t('File entity'),
      ),
    ),
    'module' => 'file_exists_condition',
  );
  return $items;
}
